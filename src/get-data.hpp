/*
 * This file is part of the Startix project
 *
 * The project is free software licensed under the GNU AGPLv3
 *
 * You can freely redistribute it and/or modify it
 * under the terms of the license agreement
 *
 * */

#pragma once

#include "commons.hpp"

string get_option(string prompt, vector<string> options, bool emtpy, const string &def);
// void get_keyboard(string &tmp, string &keymap);
void get_disk(string &disk);
void get_init(string &init);
void get_region(string &tmp, string &city);
void get_user(string &username);
void get_shell(string &shell);

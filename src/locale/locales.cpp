#include "../commons.hpp"

string get_line(string &line) {
    string tmp;

    for (unsigned int i = 1; i < line.length(); i++) {
        tmp += line[i];
    } return tmp;
}

string check_locale(const string &locale) {
    string tmp;
    ifstream file("startix/data/locale.gen");

    while(!file.eof()) {
        getline(file, tmp);

        if (tmp.find(locale) != string::npos) {
            return tmp;
        }
    }

    file.seekg(0);
    return "";
}

bool accepted_locale(const string &locale) {
    string tmp = check_locale(locale);
    if (tmp.length() > 0) {
        tmp = get_line(tmp);
        system(("echo \"" + tmp + "\" > /etc/locale.gen").c_str());
        system("locale-gen");
        return true;
    } return false;
}

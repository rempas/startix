/*
 * This file is part of the Startix project
 *
 * The project is free software licensed under the GNU AGPLv3
 *
 * You can freely redistribute it and/or modify it
 * under the terms of the license agreement
 *
 * */

#include "main.hpp"
#include "commons.hpp"
#include "get-data.hpp"

void extra(string &init, string &username) {
    string tmp;

    if (init.length() == 0)
        get_init(init);

    if (username.length() == 0)
        get_user(username);

    tmp = get_option("\nDo yo want to install ZSH and some plugins ('zsh' to only install zsh)? [y-yes/n-no/zsh] (default: yes) ",
            vector<string>{"yes", "y", "no", "n", "zsh"}, true, "yes");

    if (tmp == "zsh")
        system("yes | pacman -S zsh");
    else if (tmp == "yes" || tmp == "y")
        system("yes | pacman -S zsh zsh-completions zsh-autosuggestions zsh-syntax-highlighting zsh-history-substring-search");

    if (tmp == "yes" || tmp == "y" || tmp == "zsh") {
        system("echo zsh > startix/SHELL");
        system(("chsh --shell /bin/zsh " + username).c_str());

        system(("cp startix/data/.zshrc /home/" + username).c_str());
        system(("chown " + username + ':' + username + " /home/" + username + "/.zshrc").c_str());
    } else {
        system("echo bash > startix/SHELL");
    }

    tmp = get_option("\nDo yo want to install xorg ('nvidia' to include nvidia's proprietary drivers)? [y-yes/n-no/nvidia] (default: yes)\n",
        vector<string>{"yes", "y", "no", "n", "nvidia"}, true, "yes");

    if (tmp == "yes" || tmp == "y")
        system("yes | pacman -S xorg-server xorg-server-common");
    else if (tmp == "nvidia")
        system("yes | pacman -S xorg-server xorg-server-common nvidia");

    tmp = "";
    cout << blue + "\nDo yo want to add a window manager/desktop environment (leave it empty to not install one) " + end_color;
    getline(cin, tmp);

    if (tmp.length() > 0)
        system(("yes | pacman -S " + tmp).c_str());

    tmp = "";
    cout << blue + "\nDo yo want to add a display manager (leave it empty to not install one) " + end_color;
    getline(cin, tmp);

    if (tmp.length() > 0) {
        system(("yes | pacman -S " + tmp).c_str());

        if (init == "openrc") {
            system("rc-update add xdm default");
            system(("echo \"DISPLAYMANAGER=\"" + tmp + "\" >> /etc/conf.d/xdm").c_str());
            system(("echo \"DISPLAYMANAGER=\"" + tmp + "\" > /tmp/test").c_str());
        }
    } else {
        tmp = get_option("\nInstall xorg-xinit to automatically start your wm/de? [y-yes/n-no] (default: yes) ", y_n, true, "yes");

        if (tmp == "yes" || tmp == "y") {
            system("yes | pacman -S xorg-xinit");
            tmp = get_option("\nDo you want to automatically add .xinitrc? [y-yes/n-no] (default: yes) ", y_n, true, "yes");

            if (tmp == "yes" || tmp == "y") {
                cout << blue + "What's the binary you want to execute? " + end_color;
                getline(cin, tmp);
                system(("echo \"exec " + tmp + "\" > /home/" + username + "/.xinitrc").c_str());
                system(("chown " + username + ':' + username + " /home/" + username + "/.xinitrc").c_str());
            }

            tmp = get_option("\nDo you want to automatically start x when logging in [yes-y/n-no] (default: yes) ", y_n, true, "yes");
            if (tmp == "yes") {
                get_shell(tmp);

                if (tmp == "bash") {
                    system(("cp startix/data/.zprofile /home/" + username + "/.bash_profile").c_str());
                    system(("chown " + username + ':' + username + " /home/" + username + "/.bash_profile").c_str());
                } else {
                    system(("cp startix/data/.zprofile /home/" + username).c_str());
                    system(("chown " + username + ':' + username + " /home/" + username + "/.zprofile").c_str());
                }
            }
        }
    }

    installation_completed();
}

/*
 * This file is part of the Startix project
 *
 * The project is free software licensed under the GNU AGPLv3
 *
 * You can freely redistribute it and/or modify it
 * under the terms of the license agreement
 *
 * */

#pragma once

#include "commons.hpp"

string welcome_massage();
void help_info();
void show_locales();

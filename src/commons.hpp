/*
 * This file is part of the Startix project
 *
 * The project is free software licensed under the GNU AGPLv3
 *
 * You can freely redistribute it and/or modify it
 * under the terms of the license agreement
 *
 * */

#pragma once

#include <iostream>
#include <filesystem>
#include <fstream>
#include <unistd.h>
#include <vector>

using namespace std;
namespace fs = std::filesystem;

static string red = "\x1B[00;31m";
static string green = "\x1B[00;32m";
static string blue = "\x1B[00;34m";
static string end_color = "\x1B[0m";

static vector<string> y_n =  {"yes", "y", "no", "n"};
static vector<string> inits =  {"openrc", "runit", "s6"};
static vector<string> sections = {"START", "Set_Layout", "HELP", "Partitions", "Format", "Mount", "Internet", "Base", "Kernel", "FSTAB", "CHROOT", "Config", "Bootloader", "User", "Network", "Extra"};

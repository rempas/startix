# Startix

**Startix is no more in development** <br>
However I do consider doing bug fixes in <br>
case someone finds any. So let me know

## Latest ISO tested and working: artix-base-\*-20210101-x86\_64.iso

Startix is an installer for [Artix Linux](https://artixlinux.org) licensed under GNU AGPLv3. <br>
The goal is to make the installation process as fast, safe and flexible as possible!

At this point, Startix can't be used from absolute begginers. It is expected that you can understand <br>
and process and if you stuck, you can check the official [installation](https://wiki.artixlinux.org/Main/Installation) guide of Artix Linux. <br>
In case you are not, read the Artix documentation first and attempt to do a successful manual installation. <br>
It works without any problem but as always, you should **KEEP BACKUPS** in case something goes wrong! <br>

If that's the first time you're using Startix, have a look in the wiki for more instructions

## How to use it

1. [download](https://artixlinux.org/download.php) one of the base Artix ISOs.
2. Boot and login as root and download git: `pacman -Sy git`
3. Then clone this repo: `git clone https://gitlab.com/Rempas/startix`.
4. Copy the binary: `cp startix/startix-bin .`
5. Run the binary: `./startix-bin`

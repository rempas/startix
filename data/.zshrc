# Prompt
setopt prompt_subst
autoload -Uz vcs_info
zstyle ':vcs_info:*' stagedstr '*'
zstyle ':vcs_info:*' unstagedstr '+'
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' actionformats '%F{5}[%F{2}%b%F{3}|%F{1}%a%F{5}]%f '
zstyle ':vcs_info:*' formats \
  '%F{5}(%F{5}%b%F{5}) %F{2}%c%F{3}%u%f'
zstyle ':vcs_info:git*+set-message:*' hooks git-untracked
zstyle ':vcs_info:*' enable git
+vi-git-untracked() {
  if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] && \
  [[ $(git ls-files --other --directory --exclude-standard | sed q | wc -l | tr -d ' ') == 1 ]] ; then
  hook_com[unstaged]+='%F{1}??%f'
fi
}

precmd () { vcs_info }
# %F = set colors
# %f = reset colors
# %B = bold_colors
# %b = not_bold_colors
# %~ = working_directory
# %a = time
# %c = ~
# %d = /home/username
# %M = hostname
PROMPT='%F{2}[%F{2}%n%F{2}] %F{4}%3~ ${vcs_info_msg_0_}%F{4}->%f '

# History
HISTSIZE=30000
SAVEHIST=10000
HISTFILE=~/.cache/zsh_history

# Complition
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
# Include hidden files in autocomplete:
_comp_options+=(globdots)

# Aliases
# pacman
alias u="sudo pacman -Syu"
alias s="sudo pacman -S"
alias r="sudo pacman -Rns"
alias q="pacman -Q"
alias qi="pacman -Qi"
alias si="pacman -Si"
alias remove_orphans="sudo pacman -Rns $(pacman -Qtdq)"

# pip
alias pu="pip list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip install -U"

# git
alias gcl="git clone"
alias gi="git init"
alias gs="git status"
alias gl="git log"
alias ga="git add"
alias gaa="git add ."
alias gau="git add -u"
alias gr="git rm"
alias grd="git rm -r"
alias grd="git rm -r ."
alias gcm="git commit -m"
alias gb="git branch"
alias gbd="git branch -d"
alias gsw="git switch"
alias gc="git checkout"
alias gcb="git checkout -b"
alias gm="git merge"
alias gra="git remote add"
alias gpl="git pull"
alias gps="git push"

# tar
alias untar="tar xf"
alias untarxz="tar xf"
alias untargz="tar xzf"
alias untartgz="tar xzf"
alias untarbz2="tar xjf"
alias untartbz="tar xjf"

# other
alias l="less"
alias mf="touch"
alias md="mkdir"
alias mcd='function _mcd(){ mkdir "$1" && cd "$1" };_mcd'
alias rd="rm -r"
alias c="clear"
alias qq="exit"

# Plugins (Should be last)
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null

# History substring search
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh 2>/dev/null
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
HISTORY_SUBSTRING_SEARCH_FUZZY="yes"
HISTORY_SUBSTRING_SEARCH_ENSURE_UNIQUE=true
setopt HIST_IGNORE_ALL_DUPS
